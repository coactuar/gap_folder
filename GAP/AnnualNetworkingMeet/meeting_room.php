<?php
require_once "logincheck.php";
$curr_room = 'exhibitionhall';
?>
<?php require_once 'header.php';  ?>
<?php require_once 'preloader.php';  ?>
<div class="page-content">
    <div id="content">
        <div id="header-menu">
            <?php require_once "header-navmenu.php" ?>
        </div>
        <div id="bg">
            <img src="assets/img/MeetingRoomemial_1.jpg">

            <!-- <a href="https://player.vimeo.com/video/543013846" id="exhVideo" class="viewvideo"></a> -->
            <!-- <a href="bondk.php" id="bondk">
                <div class="indicator d-6"></div>
            </a>
            <a href="esoz.php" id="esoz">
                <div class="indicator d-6"></div>
            </a>
            <a href="colsmartA.php" id="colsmartA">
                <div class="indicator d-6"></div>
            </a>
            <a href="bonk2.php" id="bonk2">
                <div class="indicator d-6"></div>
            </a>
            <a href="lizolid.php" id="lizolid">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor.php" id="dubinor">
                <div class="indicator d-6"></div>
            </a>
            <a href="stiloz.php" id="stiloz">
                <div class="indicator d-6"></div>
            </a>
            <a href="dubinor-ointments.php" id="dubinor-ointments">
                <div class="indicator d-6"></div>
            </a>
            <a href="bmdcamp.php" id="bmdcamp">
                <div class="indicator d-6"></div>
            </a>
            <a href="ebovpg.php" id="ebovpg">
                <div class="indicator d-6"></div>
            </a>
            <a href="vkonnecthealth.php" id="vkonnecthealth">
                <div class="indicator d-6"></div>
            </a> -->
            <a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_M2YyYWQxNDktNmQ0ZS00ZWE0LWFhMGYtZjUwZjFkNzY0Y2E4%40thread.v2/0?context=%7b%22Tid%22%3a%22cf93f51c-9129-4005-85fb-c5c2df1da6b1%22%2c%22Oid%22%3a%2256a8437f-faff-400c-89ee-be05c590d132%22%7d" target="_blank" id="ebovpg">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_M2YyYWQxNDktNmQ0ZS00ZWE0LWFhMGYtZjUwZjFkNzY0Y2E4%40thread.v2/0?context=%7b%22Tid%22%3a%22cf93f51c-9129-4005-85fb-c5c2df1da6b1%22%2c%22Oid%22%3a%2256a8437f-faff-400c-89ee-be05c590d132%22%7d " target="_blank" id="bmdcamp">
                <div class="indicator d-6"></div>
            </a>
            <a href="https://teams.microsoft.com/l/meetup-join/19%3ameeting_MzNmOTgwNDYtMzk5YS00YTU5LWJlOWItMjE3NmY2YWYxOGE0%40thread.v2/0?context=%7b%22Tid%22%3a%22cf93f51c-9129-4005-85fb-c5c2df1da6b1%22%2c%22Oid%22%3a%2256a8437f-faff-400c-89ee-be05c590d132%22%7d" target="_blank" id="vkonnecthealth">
                <div class="indicator d-6"></div>
            </a>

        </div>
        <div id="bottom-menu">
            <?php require_once "bottom-navmenu.php" ?>
        </div>
    </div>
    <?php require_once "commons.php" ?>
</div>
<?php require_once "scripts.php" ?>
<script src="assets/js/image-map.js"></script>
<script>
    ImageMap('img[usemap]', 500);
</script>
<?php require_once "ga.php"; ?>

<?php require_once 'footer.php';  ?>